using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitcher : MonoBehaviour
{
    public string scene1Name; // N�zev prvn� sc�ny
    public string scene2Name; // N�zev druh� sc�ny
    public GameObject scene1Object; // Objekt v prvn� sc�n�
    public GameObject scene2Object; // Objekt v druh� sc�n�

    private bool scene1Active = true; // P�ep�na� mezi sc�nami

    // Metoda pro detekci stisknut� tla��tka ve VR
    void Update()
    {
        // P�edpokl�d�me, �e tla��tko je p�ipojeno k prvn�mu ovlada�i ve VR
        if (Input.GetButtonDown("Fire1")) // P��padn� zvolte vhodn� n�zev tla��tka
        {
            // Pokud je aktu�ln� aktivn� prvn� sc�na, p�epne na druhou
            if (scene1Active)
            {
                SwitchScene(scene2Name);
                scene1Active = false;
            }
            // Jinak p�epne na prvn� sc�nu
            else
            {
                SwitchScene(scene1Name);
                scene1Active = true;
            }
        }
    }

    // Metoda pro p�epnut� na jinou sc�nu
    public void SwitchScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);

        // Zkontroluje, zda je na�ten� sc�na 1 nebo 2 a aktivuje nebo deaktivuje odpov�daj�c� objekt
        if (sceneName == scene1Name)
        {
            scene1Object.SetActive(true);
            scene2Object.SetActive(false);
        }
        else if (sceneName == scene2Name)
        {
            scene1Object.SetActive(false);
            scene2Object.SetActive(true);
        }
    }
}
