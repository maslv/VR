using UnityEngine;
using UnityEngine.SceneManagement;

public class VRClickableObject : MonoBehaviour
{
    public SceneSwitcher sceneSwitcher; // Reference na skript SceneSwitcher
    public string sceneToSwitch; // N�zev sc�ny, na kterou se p�epne

    void Start()
    {
        // Z�sk�n� reference na SceneSwitcher skript
        sceneSwitcher = GameObject.FindObjectOfType<SceneSwitcher>();

        // Pokud nen� nastaven SceneSwitcher, vyp�e se chybov� zpr�va
        if (sceneSwitcher == null)
        {
            Debug.LogError("SceneSwitcher nebyl nalezen. P�ipojte ho pros�m do tohoto objektu.");
        }
    }

    // Metoda, kter� se zavol� p�i kliknut� na objekt ve VR
    public void OnObjectClicked()
    {
        // P�epnut� na zadanou sc�nu
        sceneSwitcher.SwitchScene(sceneToSwitch);
    }
}
